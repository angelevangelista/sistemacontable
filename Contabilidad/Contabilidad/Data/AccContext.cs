﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contabilidad.Data
{
    public class AccContext: DbContext
    {

        public AccContext()
        {

        }

        public AccContext(DbContextOptions<AccContext> options) : base(options)
        {   
        }
    }
}
